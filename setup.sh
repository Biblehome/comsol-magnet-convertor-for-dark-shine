#!/bin/bash

echo "setting ROOT ..."
source /cvmfs/sft.cern.ch/lcg/views/LCG_97rc4python3/x86_64-centos7-gcc9-opt/setup.sh

export MGCV_DS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
export MGCV_DS_INSTALL_DIR="${MGCV_DS_DIR}/../install"
echo "setting project in ${MGCV_DS_INSTALL_DIR} ..."
if [ ! -d ${MGCV_DS_INSTALL_DIR} ]; then
    echo "project run directory doesn't exsit, creating ..."
    mkdir ${MGCV_DS_INSTALL_DIR} 
fi
export PATH="${MGCV_DS_INSTALL_DIR}/:$PATH"
export LD_LIBRARY_PATH="${MGCV_DS_INSTALL_DIR}/lib:$LD_LIBRARY_PATH"
