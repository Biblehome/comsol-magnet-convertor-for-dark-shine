#ifndef MAGNET_CONSTRUCTOR_H
#define MAGNET_CONSTRUCTOR_H

#include <fstream>
#include <string>

#include "ExampleMagnet.h"

enum {Bx = 0, By = 1, Bz = 2};

class ExampleMagnetStream
{
public:
    ExampleMagnetStream(int direction = By) : name_(""), direction_(direction) {mag_.SetDirection(direction);}
    ~ExampleMagnetStream() {}

    void Open();
    void Write();
    void Close();

private:
    std::string name_;
    std::ofstream out_;
    ExampleMagnet mag_;

    int direction_;
};

#endif
