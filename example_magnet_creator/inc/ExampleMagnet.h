#ifndef MAGNET_H
#define MAGNET_H

#include <iostream>

class ExampleMagnet
{
public:
    ExampleMagnet() {}
    ~ExampleMagnet() {}

    void SetDirection(int direction) {direction_ = direction;}

    friend std::ostream &operator <<(std::ostream &os, const ExampleMagnet &mag);
    double GetExampleMagnet(double x, double y, double z);

protected:
    double Func(double x, double y, double z);

    int direction_ = 1;

    double xUp_ = 150.;
    double yUp_ = 250.;
    double zUp_ = 1080.;
    double xDown_ = -150.;
    double yDown_ = -250.;
    double zDown_ = -800.;
    double zTurning_ = 0.;

    int xBin_ = 50;
    int yBin_ = 50;
    int zBin_ = 100;
};

#endif
