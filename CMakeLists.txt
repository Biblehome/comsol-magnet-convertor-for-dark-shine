
#----------------------------------------------------------------------------
# Setup the project
#
cmake_minimum_required(VERSION 3.0 FATAL_ERROR)
project(COSMOL_magnet_convertor_for_DarkShine)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

#----------------------------------------------------------------------------
# Predefinitions
add_definitions(-DMAGNET_FILE_PATH=\"${PROJECT_SOURCE_DIR}/magnet/\")
add_definitions(-DTEST_MAGNET_FILE_X=\"${PROJECT_SOURCE_DIR}/magnet/test_magnet_x.txt\")
add_definitions(-DTEST_MAGNET_FILE_Y=\"${PROJECT_SOURCE_DIR}/magnet/test_magnet_y.txt\")
add_definitions(-DTEST_MAGNET_FILE_Z=\"${PROJECT_SOURCE_DIR}/magnet/test_magnet_z.txt\")

#----------------------------------------------------------------------------
# Find ROOT
#
find_package(ROOT)
include(${ROOT_USE_FILE})

# Find yaml-cpp
#
find_package(yaml-cpp REQUIRED)
link_libraries(yaml-cpp)

#----------------------------------------------------------------------------
# Locate sources and headers for this project
# NB: headers are included so they will show up in IDEs
#

root_generate_dictionary(G__DMagnet ${PROJECT_SOURCE_DIR}/inc/DMagnet.h LINKDEF ${PROJECT_SOURCE_DIR}/inc/DMagnetLinkDef.h)
add_library(DMagnet SHARED ${PROJECT_SOURCE_DIR}/inc/DMagnet.h ${PROJECT_SOURCE_DIR}/src/DMagnet.cpp G__DMagnet.cxx)
link_libraries(DMagnet)

include_directories(${PROJECT_SOURCE_DIR}/inc)
file(GLOB sources ${PROJECT_SOURCE_DIR}/src/*.cpp ${PROJECT_SOURCE_DIR}/src/*/*.cpp)
file(GLOB headers ${PROJECT_SOURCE_DIR}/inc/*.h   ${PROJECT_SOURCE_DIR}/inc/*/*.h)

#----------------------------------------------------------------------------
# Add the executable, and link it to the libraries
#

add_executable(magnet_convertor convertor.cpp ${sources} ${headers})
target_link_libraries(magnet_convertor ${ROOT_LIBRARIES})

#----------------------------------------------------------------------------
# Install the executable to directory defined by CMAKE_INSTALL_PREFIX and
# copy all scripts to the directory.
# This is so that we can run the executable directly because it
# relies on these scripts being in the current working directory.
#
set(CONFIG_SCRIPTS
    ${PROJECT_SOURCE_DIR}/script/config.yaml
   )

install(TARGETS magnet_convertor DESTINATION .)
install(TARGETS DMagnet LIBRARY DESTINATION lib)

#install(FILES ${CONFIG_SCRIPTS} DESTINATION .)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/libDMagnet_rdict.pcm DESTINATION lib)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/libDMagnet.rootmap DESTINATION lib)
install(FILES ${PROJECT_SOURCE_DIR}/inc/DMagnet.h DESTINATION inc)

#----------------------------------------------------------------------------
# Exmple magnet constructor
#
add_subdirectory(${PROJECT_SOURCE_DIR}/example_magnet_creator)
