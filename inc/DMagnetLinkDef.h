#ifndef D_EVENT_LINK_DEF_H
#define D_EVENT_LINK_DEF_H

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;

#pragma link C++ struct DMagnet::MagSlice+;
#pragma link C++ class std::vector<DMagnet::MagSlice>+;
#pragma link C++ class DMagnet+;

#endif

#endif
