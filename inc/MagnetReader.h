#ifndef TXT_READER_H
#define TXT_READER_H

#include <fstream>

#include "TString.h"

class MagnetReader
{
public:
    MagnetReader(TString name);
    ~MagnetReader() {this->Clear();}

    void Read(std::vector<double> &x, std::vector<double> &y, std::vector<double> &z, std::vector<double> &B);

private:
    void Clear() {}

    TString name_;
    std::ifstream file_;
};

#endif
